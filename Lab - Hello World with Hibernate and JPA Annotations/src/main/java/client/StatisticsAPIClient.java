package client;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;

import entity.Guide;

public class StatisticsAPIClient {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hello-world");

        Statistics stats = emf.unwrap(SessionFactory.class).getStatistics();    // create statistics
        stats.setStatisticsEnabled(true);                                       // enable

        EntityManager em1 = emf.createEntityManager();
        em1.getTransaction().begin();

        Guide guide1 = em1.find(Guide.class, 2L);

        em1.getTransaction().commit();
        em1.close();

        EntityManager em2 = emf.createEntityManager();
        em2.getTransaction().begin();

        Guide guide2 = em2.find(Guide.class, 2L);

        em2.getTransaction().commit();
        em2.close();

        System.out.println(stats.getSecondLevelCacheStatistics("entity.Guide"));    //show STATISTICS
        //SecondLevelCacheStatistics[hitCount=1,missCount=1,putCount=1,elementCountInMemory=1,elementCountOnDisk=0,sizeInMemory=2188]
        // missCount when do not find data in second level cache
        //putCount  when data are loaded to SECOND LEVEL CACHE from DATABASE
        // hitCount when element is found in second level cache (cache head)
        //WHEN ADD SECOND LEVEL CACHE REMEMBER it SHOULD BE MANY hitCount AND only few missCount
        // we are using sizeInMemory BYTES of memory
    }
}














